package com.rockbass2560.prueba.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rockbass2560.prueba.R;
import com.rockbass2560.prueba.models.TestResponse;

import java.util.ArrayList;
import java.util.List;

public class TestDataAdapter extends RecyclerView.Adapter<TestDataAdapter.HolderData> {

    private List<TestResponse.Example> exampleList = new ArrayList<>();

    public void addData(List<TestResponse.Example> exampleList) {
        this.exampleList.addAll(exampleList);
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.data_card, parent, false);
        return new HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        TestResponse.Example data = exampleList.get(position);
        holder.bind(data);
    }

    @Override
    public int getItemCount() {
        return exampleList.size();
    }

    static class HolderData extends RecyclerView.ViewHolder {

        private TextView mTitle, mContent;

        public HolderData(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.card_label_title);
            mContent = itemView.findViewById(R.id.card_label_content);
        }

        public void bind(TestResponse.Example data) {
            mTitle.setText(data.getTitle());
            mContent.setText(data.getContent());
        }
    }
}
