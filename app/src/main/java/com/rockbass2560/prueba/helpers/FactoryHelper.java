package com.rockbass2560.prueba.helpers;

import android.app.Activity;
import android.app.Application;

import androidx.lifecycle.ViewModelProvider;

public class FactoryHelper {

    public static ViewModelProvider.AndroidViewModelFactory getDefaultFactory(Activity activity) {
        return getDefaultFactory(activity.getApplication());
    }

    public static ViewModelProvider.AndroidViewModelFactory getDefaultFactory(Application application){
        return ViewModelProvider.AndroidViewModelFactory.getInstance(application);
    }

}
