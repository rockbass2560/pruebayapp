package com.rockbass2560.prueba.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rockbass2560.prueba.net.RetrofitRepositories;
import com.rockbass2560.prueba.net.TestService;
import com.rockbass2560.prueba.models.User;
import com.rockbass2560.prueba.models.LoginResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginResult> resultLogin = new MutableLiveData<>();

    public LiveData<LoginResult> getValidaLogin(){
        return resultLogin;
    }

    //No venía ningun usuario en el documento...
    public void validLogin(User user){
        LoginResult loginResult;

        if (user.getUser().isEmpty()){
            loginResult = new LoginResult();
            loginResult.setError(true);
            loginResult.setMessage("Ingrese un usuario por favor");
            resultLogin.postValue(loginResult);
        }else if (user.getPassword().isEmpty()){
            loginResult = new LoginResult();
            loginResult.setError(true);
            loginResult.setMessage("Ingrese una contraseña por favor");
            resultLogin.postValue(loginResult);
        }else { //Solicitud
            TestService testService = RetrofitRepositories.getTestService();
            testService.login(user).enqueue(new Callback<LoginResult>() {
                @Override
                public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
                    resultLogin.postValue(response.body());
                }

                @Override
                public void onFailure(Call<LoginResult> call, Throwable t) {
                    errorSolicitud();
                }
            });
        }
    }

    private void errorSolicitud() {
        LoginResult loginResult = new LoginResult();
        loginResult.setError(true);
        loginResult.setMessage("Ocurrió un error en la solicitud");

        resultLogin.postValue(loginResult);
    }

}