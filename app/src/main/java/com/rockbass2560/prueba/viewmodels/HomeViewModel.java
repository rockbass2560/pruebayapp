package com.rockbass2560.prueba.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rockbass2560.prueba.net.RetrofitRepositories;
import com.rockbass2560.prueba.models.TestResponse;
import com.rockbass2560.prueba.models.TestResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {
    private MutableLiveData<TestResult> testLiveData = new MutableLiveData<>();

    public LiveData<TestResult> getTestResultLiveData(){
        return testLiveData;
    }

    public void getTestData() {
        RetrofitRepositories.getTestService().getTest().enqueue(
                new Callback<TestResponse>() {
                    @Override
                    public void onResponse(Call<TestResponse> call, Response<TestResponse> response) {
                        if (response.isSuccessful()) {
                            TestResult testResult = new TestResult();
                            testResult.setError(false);
                            testResult.setTestResponse(response.body());
                            testLiveData.postValue(testResult);
                        } else {
                            errorEnPeticion();
                        }
                    }

                    @Override
                    public void onFailure(Call<TestResponse> call, Throwable t) {
                        errorEnPeticion();
                    }
                }
        );
    }

    private void errorEnPeticion(){
        TestResult testResult = new TestResult();
        testResult.setError(true);
        testResult.setMessage("Ocurrió un error al tratar de recibir los datos");

        testLiveData.postValue(testResult);
    }
}