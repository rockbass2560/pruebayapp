package com.rockbass2560.prueba;

public class Constants {
    public final static String NAME_SHARED = "name";
    public final static String SHARED_DATA = "shared_data";
    public final static String URL_BASE = "http://3.94.51.72/skillTest/";
    public final static String PHONE_SHARE = "phone";
    public final static String BIRTHDATE_SHARE = "birthdate";

    public final static String EDITED = "edited";

    public final static String LAT = "lat";
    public final static String LNG = "lng";
}
