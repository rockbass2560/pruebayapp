package com.rockbass2560.prueba.views;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rockbass2560.prueba.Constants;
import com.rockbass2560.prueba.R;

public class MapsMarkerActivity extends AppCompatActivity
        implements OnMapReadyCallback {

    LatLng position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_marker);

        Intent intent = getIntent();

        if (intent!=null) {


            Bundle extras = getIntent().getExtras();
            double lat = extras.getDouble(Constants.LAT, -33.852);
            double lng  = extras.getDouble(Constants.LNG, 151.211);

            position = new LatLng(lat, lng);

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            finish();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(position)
                .title("Your position"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
    }
}
