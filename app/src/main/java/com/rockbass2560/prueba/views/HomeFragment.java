package com.rockbass2560.prueba.views;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rockbass2560.prueba.Constants;
import com.rockbass2560.prueba.adapters.TestDataAdapter;
import com.rockbass2560.prueba.helpers.FactoryHelper;
import com.rockbass2560.prueba.models.TestResult;
import com.rockbass2560.prueba.viewmodels.HomeViewModel;
import com.rockbass2560.prueba.R;

public class HomeFragment extends Fragment {

    private HomeViewModel mViewModel;
    private String name;
    private TestDataAdapter adapter;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem misDatos = menu.add("Mis Datos");
        MenuItem cerrarSesion = menu.add("Cerrar Sesion");

        misDatos.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                InputDialog.createDialog(getActivity(), getChildFragmentManager())
                    .show();
                return true;
            }
        });

        cerrarSesion.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_DATA, 0);
                sharedPreferences.edit().clear().apply();
                NavController navController = NavHostFragment.findNavController(HomeFragment.this);
                navController.navigate(R.id.action_homeFragment_to_loginFragment);
                return true;
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        TextView labelName = view.findViewById(R.id.home_label_name);
        labelName.setText(name);

        RecyclerView recyclerView = view.findViewById(R.id.home_recyclerview_data);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TestDataAdapter();
        recyclerView.setAdapter(adapter);

        FloatingActionButton fabMap = view.findViewById(R.id.home_button_map);
        fabMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeFragment.this.getActivity(), MapsMarkerActivity.class);
                Bundle extras = new Bundle();
                MainActivity activity = (MainActivity)HomeFragment.this.getActivity();
                Location location = activity.getLastLocation();
                extras.putDouble(Constants.LAT, location.getLatitude());
                extras.putDouble(Constants.LNG, location.getLongitude());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        return view;
    }

    private Observer<TestResult> testResultObserver = new Observer<TestResult>() {
        @Override
        public void onChanged(TestResult testResult) {
            if (!testResult.isError()) {
                adapter.addData(testResult.getTestResponse().getData().getExampleList());
                adapter.notifyDataSetChanged();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setTitle("Ocurrió un error")
                        .setMessage(testResult.getMessage())
                        .setPositiveButton("Ok", null)
                        .show();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_DATA, 0);
        name = sharedPreferences.getString(Constants.NAME_SHARED, "");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = getDefaultViewModelProviderFactory().create(HomeViewModel.class);
        mViewModel.getTestResultLiveData().observe(getViewLifecycleOwner(), testResultObserver);
        mViewModel.getTestData();
    }
}