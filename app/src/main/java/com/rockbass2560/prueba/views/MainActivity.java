package com.rockbass2560.prueba.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.rockbass2560.prueba.R;

public class MainActivity extends AppCompatActivity {

    private static int LOCATION_RESULT = 10;
    private Location lastLocation = null;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_RESULT) {
            if (permissions.length > 0) {
                doRequestLocations();
            } else {
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setMessage("Por favor habilitar los permisos de localización")
                        .show();
            }
        }
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (thereIsLocationPermission()) {
            //Aqui vamos a obtener la localizacion
            doRequestLocations();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, LOCATION_RESULT);
        }
    }

    private boolean thereIsLocationPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("MissingPermission")
    private void doRequestLocations() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getProviders(true).get(0);
        locationManager.requestLocationUpdates(
                provider,
                5000,
                1,
                new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        lastLocation = location;
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) { }

                    @Override
                    public void onProviderEnabled(String provider) { }

                    @Override
                    public void onProviderDisabled(String provider) { }
                }
        );
    }
}