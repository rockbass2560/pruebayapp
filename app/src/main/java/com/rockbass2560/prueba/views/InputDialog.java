package com.rockbass2560.prueba.views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.rockbass2560.prueba.Constants;
import com.rockbass2560.prueba.R;

import java.util.Calendar;
import java.util.Date;

public class InputDialog implements DatePickerDialog.OnDateSetListener {

    private TextInputEditText phoneText;
    private TextInputEditText birthdateText;
    private TextInputEditText nameText;
    private ImageButton buttonCalendar;
    private int age;
    private SharedPreferences sharedPreferences;
    private FragmentActivity activity;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;
    private FragmentManager fragmentManager;

    public static AlertDialog createDialog(FragmentActivity activity, FragmentManager fragmentManager) {
        InputDialog inputDialog = new InputDialog(activity, fragmentManager);
        return inputDialog.getAlertDialog();
    }

    public AlertDialog getAlertDialog(){
        return alertDialog;
    }

    private InputDialog(FragmentActivity activity, FragmentManager fragmentManager){
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        sharedPreferences = activity.getSharedPreferences(Constants.SHARED_DATA, 0);
        boolean isEdited = sharedPreferences.getBoolean(Constants.EDITED, false);

        View view = LayoutInflater.from(activity).inflate(R.layout.info_dialog, null);

        birthdateText = view.findViewById(R.id.dialogdata_input_birthdate);
        phoneText = view.findViewById(R.id.dialogdata_input_phone);
        nameText = view.findViewById(R.id.dialogdata_input_name);
        buttonCalendar = view.findViewById(R.id.dialogdata_input_imagebirthdate);

        String name = sharedPreferences.getString(Constants.NAME_SHARED, "");
        nameText.setText(name);

        alertDialogBuilder = new AlertDialog.Builder(activity)
                .setTitle("Mis Datos")
                .setView(view);

        if (isEdited) {
            isNotEditig();
        } else {
            isEditing();
        }
    }

    private void isNotEditig() {
        alertDialog = alertDialogBuilder.setPositiveButton("Cerrar", null)
                .create();

        String phone = sharedPreferences.getString(Constants.PHONE_SHARE, "");
        String birthdate = sharedPreferences.getString(Constants.BIRTHDATE_SHARE, "");

        birthdateText.setText(birthdate);
        phoneText.setText(phone);
        phoneText.setEnabled(false);

        new AlertDialog.Builder(activity)
                .setMessage("Los datos ya han sido llenados")
                .show();
    }

    private void isEditing(){
        alertDialog = alertDialogBuilder
                .setPositiveButton("Aceptar", null)
                .create();

        buttonCalendar.setOnClickListener(v -> {
            DialogDate dialogDate = new DialogDate(v.getContext(), InputDialog.this);
            dialogDate.show(fragmentManager, "pick_date");
        });

        alertDialog.setOnShowListener(dialog -> {
            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view -> {
                if (phoneText.getText().toString().length() < 10){
                    Toast.makeText(view.getContext(), "El telefono debe ser de 10 digitos", Toast.LENGTH_LONG).show();
                    return;
                }
                if (birthdateText.getText().toString().isEmpty()){
                    Toast.makeText(view.getContext(), "Fecha de cumpleaños incorrecta", Toast.LENGTH_LONG).show();
                    return;
                }

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.PHONE_SHARE, phoneText.getText().toString());
                editor.putString(Constants.BIRTHDATE_SHARE, Integer.toString(age));
                editor.putBoolean(Constants.EDITED, true);
                editor.apply();

                new AlertDialog.Builder(view.getContext())
                        .setMessage(
                                String.format("Datos guardados\nNombre: %s\nTelefono: %s\nEdad: %s",
                                        nameText.getText().toString(),
                                        phoneText.getText().toString(),
                                        age)
                        ).show();
                dialog.dismiss();
            });
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        age = currentYear - year;
        birthdateText.setText(String.format("%s/%s/%s", dayOfMonth, month, year));
    }

    public static class DialogDate extends DialogFragment {

        private Context context;
        private DatePickerDialog.OnDateSetListener listener;

        public DialogDate(Context context, DatePickerDialog.OnDateSetListener listener){
            this.context = context;
            this.listener = listener;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(context, listener, year, month, day);
        }
    }
}
