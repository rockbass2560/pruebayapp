package com.rockbass2560.prueba.views;

import androidx.appcompat.app.AlertDialog;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.rockbass2560.prueba.Constants;
import com.rockbass2560.prueba.R;
import com.rockbass2560.prueba.models.LoginResult;
import com.rockbass2560.prueba.models.User;
import com.rockbass2560.prueba.viewmodels.LoginViewModel;

public class LoginFragment extends Fragment {

    private LoginViewModel mViewModel;
    private TextInputEditText mInputUser, mInputPassword;
    private MaterialButton mLoginButton;
    private User user;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        mLoginButton = view.findViewById(R.id.login_button_login);
        mInputUser = view.findViewById(R.id.login_input_user);
        mInputPassword = view.findViewById(R.id.login_input_password);

        mLoginButton.setOnClickListener(clickLogin);

        return view;
    }

    View.OnClickListener clickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String userStr = mInputUser.getText().toString();
            String password = mInputPassword.getText().toString();

            user = new User();
            user.setUser(userStr);
            user.setPassword(password);

            mViewModel.validLogin(user);
        }
    };

    private Observer<LoginResult> loginResultObserver = new Observer<LoginResult>() {
        @Override
        public void onChanged(LoginResult loginResult) {
            if (!loginResult.isError()) {
                NavController navController = NavHostFragment.findNavController(LoginFragment.this);

                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_DATA,0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.NAME_SHARED, user.getName());
                editor.apply();

                navController.navigate(R.id.action_loginFragment_to_homeFragment);

            } else {
                AlertDialog alertDialog = new MaterialAlertDialogBuilder(getContext())
                        .setTitle("Ocurrió un error")
                        .setMessage(loginResult.getMessage())
                        .setPositiveButton("Ok", null)
                        .show();
            }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = getDefaultViewModelProviderFactory().create(LoginViewModel.class);
        mViewModel.getValidaLogin().observe(getViewLifecycleOwner(), loginResultObserver);
    }

}