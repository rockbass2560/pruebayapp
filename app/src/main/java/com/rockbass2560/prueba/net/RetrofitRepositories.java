package com.rockbass2560.prueba.net;

import com.rockbass2560.prueba.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRepositories {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static TestService testService;

    public static TestService getTestService() {
        if (testService==null){
            testService = retrofit.create(TestService.class);
        }

        return testService;
    }
}
