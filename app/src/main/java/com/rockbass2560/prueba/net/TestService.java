package com.rockbass2560.prueba.net;

import com.rockbass2560.prueba.models.LoginResult;
import com.rockbass2560.prueba.models.TestResponse;
import com.rockbass2560.prueba.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TestService {

    @POST("testLogin")
    Call<LoginResult> login(@Body User user);

    @GET("testContent")
    Call<TestResponse> getTest();
}
