package com.rockbass2560.prueba.models;

public class User {
    private String user;
    private String password;
    //No se indico de donde se obtiene el nombre
    private String name;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
        //El nombre será el nombre de usuario por ahora
        this.name = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
