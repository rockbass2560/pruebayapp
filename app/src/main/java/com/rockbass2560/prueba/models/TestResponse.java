package com.rockbass2560.prueba.models;

import java.util.List;

public class TestResponse {
    private Data data;

    public Data getData(){
        return data;
    }

    public void setData(Data data){
        this.data = data;
    }

    public static class Data {
        private int status;
        private String generalTitle;
        private List<Example> exampleList;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getGeneralTitle() {
            return generalTitle;
        }

        public void setGeneralTitle(String generalTitle) {
            this.generalTitle = generalTitle;
        }

        public List<Example> getExampleList() {
            return exampleList;
        }

        public void setExampleList(List<Example> exampleList) {
            this.exampleList = exampleList;
        }
    }

    public static class Example
    {
        private String title;
        private String content;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
