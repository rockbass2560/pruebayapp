package com.rockbass2560.prueba.models;

public class TestResult {
    private String message;
    private boolean error;
    private TestResponse testResponse;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public TestResponse getTestResponse() {
        return testResponse;
    }

    public void setTestResponse(TestResponse testResponse) {
        this.testResponse = testResponse;
    }
}
