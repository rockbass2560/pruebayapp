package com.rockbass2560.prueba.models;

public class LoginResult {
    private boolean error;
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error){this.error = error;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message){this.message = message;}
}
